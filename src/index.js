const express = require('express');
const path = require('path');   //con esto, es multiplataforma, pq me da la direccion
const engine = require('ejs-mate');
const flash = require('connect-flash');
const session = require('express-session');
const passport = require('passport');
const morgan = require('morgan');
const helmet = require('helmet');
const rateLimit = require("express-rate-limit");
var MongoStore = require('rate-limit-mongo');
const mongoSanitize = require('express-mongo-sanitize');  //evita que se envie cosas con $ que es propio de mongo
const hpp = require('hpp');
const { mongodb, sessions } = require('./keys');
// const { sessions } = require('./keys');


// initializations
const app = express();
require('./database');
require('./passport/local-auth'); // le decimos donde esta passport
// const apiLimiter = rateLimit({  //LIMITADOR DE 60 CONSULTAS EN 15 MINTUOS PARA TODA LA API
//   windowMs: 15 * 60 * 1000, // 15 minutes
//   max: 60
// });
var apiLimiter = new rateLimit({
  store: new MongoStore({
    uri: mongodb.URI_APILIMITER,
    // collection: 'expressRateRecords-api'
  }),
  max: 100,
  windowMs: 15 * 60 * 1000
});

// settings
app.set('port', process.env.PORT || 3000);
app.set('views', path.join(__dirname, '/views/'))     //le indico donde esta la carpeta views
app.engine('ejs', engine);  //motor de plantillas para el HTML
app.set('view engine', 'ejs');

// app.use('/static', express.static('views/layouts/js'))
app.use(express.static(__dirname + '/views/'));
app.use(express.static(__dirname + '/views/layouts/'));
app.use(express.static(__dirname + '/views/mapas/'));

// app.use(express.static(path.join(__dirname, "js")));

// middlewares  - se ejecutan antes de llegar a la ruta
app.use(morgan('dev'));
app.use(express.urlencoded({ extended: true })); //nos permite recibir datos simples (no pesados) del cliente
app.use(session({
  secret: sessions.SECRETO,
  resave: false,
  saveUninitialized: false,
  rolling: true, // <-- Set `rolling` to `true`
  maxAge: 1*60*60*1000
  // cookie: { secure: true }
}));
app.use(flash());   // es otro middel  para manejar mensajes
app.use(passport.initialize());
app.use(passport.session());  // se lo pongo aca pq es un middelware y se debe usar aca. 

app.use(mongoSanitize());
app.use(hpp());
app.use(helmet());
app.use(helmet.contentSecurityPolicy({
  directives: {
    defaultSrc: ["'self'", "172.30.180.10:3025/find/estacion/", "200.16.75.240:3025/find/estacion/"],  //es para los SRC de las imagenes
    scriptSrc: ["'self' 'unsafe-inline'", "code.jquery.com/", "cdn.jsdelivr.net/", "cdnjs.cloudflare.com","stackpath.bootstrapcdn.com/", "172.30.180.10:3025/find/estacion/"], 
    styleSrc: ["'self' 'unsafe-inline'", "bootswatch.com/"], //el self es para cargar los archivos locales de style
    imgSrc: ["'self' data:"], 
    fontSrc: ["'self'"],
  }
}));


app.use((req, res, next) => {   // genero mi propio middelware para mostrar el mensaje cuando existe el email
  app.locals.signinMessage = req.flash('signinMessage');
  app.locals.signupMessage = req.flash('signupMessage');
  app.locals.user = req.user;
  // console.log(app.locals)
  next(); // es para que siga abajo
});

// routes
app.use('/', apiLimiter, require('./routes/index'));    //le digo a express que use esta ruta cada vez que se abre la pagina principal

// Starting the server
app.listen(app.get('port'), () => {
  console.log('Server running on port: ', app.get('port'));
  // console.log("HOLA "+__dirname)
});
