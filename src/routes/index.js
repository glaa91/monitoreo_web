const router = require('express').Router();
const passport = require('passport');
const rateLimit = require("express-rate-limit");
// const fetch = require("node-fetch"); 
const request = require('request');
var MongoStore = require('rate-limit-mongo');
const { mongodb } = require('./../keys');

//LIMITADOR DE PREGUNTAS
const createAccountLimiter = rateLimit({
  windowMs: 60 * 60 * 1000, // 1 hour window
  max: 2, // start blocking after 5 requests
  message: "Demasiadas cuentas creadas desde esta IP, intentá nuevamente en un rato..."
});
var signUpLimiter = new rateLimit({
  store: new MongoStore({
    uri: mongodb.URI_SIGNUPLIMITER,
    // collection: 'expressRateRecords-signup'
  }),
  max: 2,
  windowMs: 1 * 60 * 1000,  //1 CUENTA POS MINUTO
  expireTimeMs: 2 * 60 * 1000,  //CADA 2 MINUTOS DE VUELVE A HABILITAR
  message: "Demasiadas cuentas creadas desde esta IP, intentá nuevamente en un rato..."
});

router.get('/', (req, res, next) => {
  res.render('signin');  //con esto le digo que vaya a views y levante el main.ejs
  // res.send("HOLA")
});

router.get('/signup', (req, res, next) => { //envia la ventana para loguearse
  res.render('signup');
});

router.post('/signup', signUpLimiter, passport.authenticate('local-signup', {  //escucha los datos que envia el user del get
  // le digo que usse el meotod local-signup que cree en routes ara auutennticarse
  successRedirect: '/main',  //ME LLEVA AL MAPA
  failureRedirect: '/signup', //si falla que vuelva a signup
  failureFlash: true
}));

router.get('/signin', (req, res, next) => {
  res.render('signin');
});


router.post('/signin', passport.authenticate('local-signin', {
  successRedirect: '/main',   //ME LLEVA AL MAPA
  failureRedirect: '/signin',
  failureFlash: true
}));

//LO QUE PUEDO HACER ACA ES AGREGAR OTRO MIDDELWARE PARA PROTEGER TODOS LOS LINKS DE ACA ABAJO, Y QUE SI NO ESTE AUTENTICADO NI
//  LOS MUESTRE.

//antes de pasar a las rutas de abajo, tiene que pasar por el metodo use, y el metodo use siempre se ejecuta antes de  pasar a la siguiente
router.use((req, res, next) => {
  isAuthenticated(req, res, next)
  next();
})

router.get('/main', (req, res, next) => {
  res.render('mapa');
});

router.get('/profile', (req, res, next) => {
  res.render('profile');
});

router.get('/dashboard', (req, res, next) => {
  res.render('dashboard')
})

router.get('/logout', (req, res, next) => {
  req.logout();
  res.redirect('/');
});

// router.get('/update/estacion/:id_estacion', (req, res) => {
//   const _estacion = req.params.id_estacion;
//   console.log("--> /update/estacion/" + _estacion);
//   const url = "http://172.30.180.10:3025/find/estacion/" + _estacion;


//   request(url, (err, res1, body) => {
//         if (err) 
//             return console.log(err); 
//         else{
//             //console.log(body);
//             return res.send(body);
//         }
//     });


// });

router.get('/mapas/:id_estacion', (req, res, next) => {
  let _estacion = req.params.id_estacion;
  let test_number = /^\d+$/;

  if (test_number.test(_estacion)) {
    console.log("--> /mapas/" + _estacion, + " --  esNumeroValido? " + test_number.test(_estacion));
    res.render('mapas/mapa_' + _estacion)
  } else {
    console.log("--> /mapas/" + _estacion, + " --  INVALIDO " + test_number.test(_estacion));
    res.redirect('/main');
  }
})

router.get('/metricas', isLevel9, (req, res, next) => {
  // res.send(req.user.nivel);
  res.render('metricas')
})

function isAuthenticated(req, res, next) {  //creo in middelware para ver si el usuario esta autenticado cada vez que va a alguna pagina
  if (req.isAuthenticated()) {
    return next();  //si lo esta que continue
  }
  res.redirect('/') //sino que vaya a loguearse
}

function isLevel9(req, res, next) {
  if (req.user.nivel == 9) {
    return next();
  } else {
    res.status('401').redirect('/main');
  }
}


module.exports = router;