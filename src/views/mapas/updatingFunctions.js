// const APIURL = "/update/estacion/";

//IP INTERNA
const APIURL = "http://172.30.180.10:3025/find/estacion/";
// IPexterna 
// APIURL = "http://200.16.75.240:3025/find/estacion/";


//Event Listeners

// document.getElementById("switchFav_110")

// funcion para cargar los datos cada X segundos
async function updateStation(idEstacion) {
  console.log("Consultando por IdEstacion: " + idEstacion);
  const url = APIURL + idEstacion;
  // const res = await httpGet(url);
  fetch(url)
 
  .then(response => response.json())
  .then(data => {

    const _aux = data;
    var actualTime = Date.now();
    for (i in _aux) {
      if (_aux[i] !== undefined) {
        if (_aux[i].ultima_conexion_servicio == "null") {
          $('#' + _aux[i].nombre).css({ fill: "#6A6A6A" });
        } else if ((actualTime - new Date(_aux[i].ultima_conexion_servicio)) > 480000) {
          $('#' + _aux[i].nombre).css({ fill: "#000000" });
        } else {
          if (_aux[i].estado == true) $('#' + _aux[i].nombre).css({ fill: "#00ff00" });
          else {
            $('#' + _aux[i].nombre).css({ fill: "#ff0000" });
          }
        }
        if (_aux[i].tipo === "VAL") {
          $('#' + _aux[i].nombre).find("title").html("Equipo: " + _aux[i].nombre + " - " + _aux[i].referencia + " - SN: " + _aux[i].serialNumber +
            "\n" + "Operativo? " + _aux[i].estado + "\n" + "IP: " + _aux[i].ip + "\n \n" + "Ultima Comunc. Servicio: " + _aux[i].ultima_conexion_servicio + "\n" +
            "Ultima Comunc. Val: " + _aux[i].ultima_conexion_val + "\n \n"
            + "Tiempo Activo: " + _aux[i].salud.tiempoActividad + "min. ("
            + (_aux[i].salud.tiempoActividad / 1440).toFixed(1) + "dias)" + "\n \n" + "Contadores:  OUT:" + _aux[i].contadores.out +
            " - IN:" + _aux[i].contadores.in + " - FRAUD:" + _aux[i].contadores.fraude + "\n \n" + "Salud: TEMP:" +
            _aux[i].salud.temp + " - FIRM: " + _aux[i].salud.firm + " - PROCESOS: " + _aux[i].salud.procesos +
            " - ESPA. DISCO: " + _aux[i].salud.espacioDisco + "\n \n" + "PerdidaRed: " + _aux[i].red.fechaPerdidaRed +
            " - RecuperoRed: " + _aux[i].red.fechaRecuperaRed + "\n \n" +
            "Ultima Inicial. " + _aux[i].red.ultimaInicializacion + " - Ultima Update. " + _aux[i].red.ultimaActualizacion)

          if (_aux[i].ultima_conexion_val == "null") {
            $('#' + _aux[i].nombre).css({ fill: "#000000" });
          } else if ((actualTime - new Date(_aux[i].ultima_conexion_val)) > 900000) {  //15 min
            $('#' + _aux[i].nombre).css({ fill: "#000000" });
          }

        } else if (_aux[i].tipo === "GATE") {
          $('#' + _aux[i].nombre).find("title").html("Equipo: " + _aux[i].nombre + "\n" + "Operativo? " + _aux[i].estado + "\n"
            + "LastCom Servicio: " + _aux[i].ultima_conexion_servicio + "\n \n" + "Contadores: \n" + "  Autoriz. A: " + _aux[i].contadores.NUMBER_AUTHORIZATIONS_A +
            "\n  Pasajeros. A: " + _aux[i].contadores.NUMBER_PASSAGES_A + "\n  Pasajeros. B: " + _aux[i].contadores.NUMBER_PASSAGES_B +
            "\n  Intrusion. A: " + _aux[i].contadores.INTRUSION_A + "\n  Fraude. A: " + _aux[i].contadores.FRAUD_A +
            "\n  Fraude. B: " + _aux[i].contadores.FRAUD_B)

        } else if (_aux[i].tipo === "SEM") {
          $('#' + _aux[i].nombre).find("title").html("Equipo: " + _aux[i].nombre + "\n" + "Operativo? " + _aux[i].estado + "\n"
            + "LastCom Servicio: " + _aux[i].ultima_conexion_servicio + "\n" + "LastCom Cartel: " + _aux[i].ultima_conexion_sem);
        } else {
          $('#' + _aux[i].nombre).find("title").html("Equipo: " + _aux[i].nombre + "\n" + "Operativo? " + _aux[i].estado + "\n" + "LastCom Servicio: " + _aux[i].ultima_conexion_servicio)
        }
      }
    }
    
    
  })
  .catch(error => {
    console.error(error)
    window.alert('No se pudo cargar datos de la API.');
  });

}

// funcion para cargar los datos al hacer click
function on(clicked_id) {
  console.log("IdEsquipo: " + clicked_id)
  // document.getElementById("text").innerHTML = clicked_id;
  document.getElementById("overlay").style.display = "block";

  const url = "/mapaSVG/updateData/nombre/" + clicked_id

  // let _aux = JSON.parse( httpGet(url));
  let texto = clicked_id;
  // let i=0;
  // if(_aux[i].tipo === "VAL"){
  //   texto = "Equipo: "+_aux[i].nombre + " - "+_aux[i].referencia + " - SN: "+ _aux[i].serialNumber  + "\n"+"Operativo? "
  //           + _aux[i].estado + "\n" +"IP: "+ _aux[i].ip + "\n \n"+ "Ultima Comunc. Servicio: "+  _aux[i].ultima_conexion_servicio  
  //           + "\n" + "Ultima Comunc. Val: " + _aux[i].ultima_conexion_val + "\n \n"  + "Tiempo Activo: "+ 
  //           _aux[i].salud.tiempoActividad + "min. (" + (_aux[i].salud.tiempoActividad/1440).toFixed(1) + "dias)" + "\n \n" + 
  //           "Contadores:  OUT:" + _aux[i].contadores.out +  " - IN:" + _aux[i].contadores.in + " - FRAUD:" + 
  //           _aux[i].contadores.fraude + "\n \n" + "Salud: TEMP:" +  _aux[i].salud.temp + " - FIRM: " + _aux[i].salud.firm + 
  //           " - PROCESOS: " + _aux[i].salud.procesos + " - ESPA. DISCO: " + _aux[i].salud.espacioDisco +  "\n \n" + "PerdidaRed: "
  //           + _aux[i].red.fechaPerdidaRed + " - RecuperoRed: " + _aux[i].red.fechaRecuperaRed + "\n \n" + "Ultima Inicial. "
  //           + _aux[i].red.ultimaInicializacion + " - Ultima Update. "+ _aux[i].red.ultimaActualizacion;
  // }else if(_aux[i].tipo === "GATE"){
  //   texto =  "Equipo: "+_aux[i].nombre + "\n"+"Operativo? "+ _aux[i].estado + "\n" 
  //           +"LastCom Servicio: "+_aux[i].ultima_conexion_servicio + "\n \n" + "Contadores: \n" + "  Autoriz. A: " + 
  //           _aux[i].contadores.NUMBER_AUTHORIZATIONS_A + "\n  Pasajeros. A: " + _aux[i].contadores.NUMBER_PASSAGES_A + 
  //           "\n  Pasajeros. B: " + _aux[i].contadores.NUMBER_PASSAGES_B + "\n  Intrusion. A: " + _aux[i].contadores.INTRUSION_A + 
  //           "\n  Fraude. A: " + _aux[i].contadores.FRAUD_A + "\n  Fraude. B: " + _aux[i].contadores.FRAUD_B;
  // }else if(_aux[i].tipo === "SEM"){
  //   texto =  "Equipo: "+_aux[i].nombre + "\n"+"Operativo? "+ _aux[i].estado + "\n"
  //           +"LastCom Servicio: "+_aux[i].ultima_conexion_servicio + "\n" +"LastCom Cartel: "+_aux[i].ultima_conexion_sem;
  // }
  document.getElementById("text").innerHTML = texto;

}

// async function httpGet(theUrl) {
//   return new Promise( (resolve, reject) => {
//     const timer = setTimeout(() => {
//       reject(new Error('TIMEOUT'))
//     }, ms)
//     const res = await fetch(url);
//   });
  
// }

function off() {
  document.getElementById("overlay").style.display = "none";
}
