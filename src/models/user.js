const mongoose = require('mongoose');
const bcrypt = require('bcrypt-nodejs');

const { Schema } = mongoose;

const userSchema = new Schema({
  email: {
    type: String,
    required: true,
    unique: true
  },
  password: {
    type: String,
    required: true
  },
  legajo: {
    type: String,
    required: true,
    unique: true
  },
  nombre : {
    type: String,
    required: true
  },
  apellido: {
    type: String,
    required: true
  },
  gerencia: {
    type: String,
    required: true
  },
  nivel : {
    type: String,
    default: 1
  },
  creado: {
    type: Date,
    default: Date.now()
  },
  estaciones_fav: Array
});

userSchema.methods.encryptPassword = (password) => {    //para guardar
  return bcrypt.hashSync(password, bcrypt.genSaltSync(10));
};

userSchema.methods.comparePassword= function (password) {   //para comparar al comparar
  return bcrypt.compareSync(password, this.password);
};

module.exports = mongoose.model('user', userSchema);