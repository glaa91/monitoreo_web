const passport = require('passport');
const LocalStrategy = require('passport-local').Strategy;

const User = require('../models/user');

passport.serializeUser((user, done) => {  //esto es para ir guardando el user y que se recuerde para no tener que loguearse en cada pagina
  done(null, user.id);  // guardo solo el ID
});

passport.deserializeUser(async (id, done) => {
  const user = await User.findById(id); // es para enviar el id guardado y que passport lo vea y lo analice (por eso lo busco)
  done(null, user);
});

passport.use('local-signup', new LocalStrategy({    // creo un metodo para autenticar con passport
  //objeto de configuracion y callback para avisarle al cliente
  usernameField: 'email',   // son los nombres de form
  passwordField: 'password',
  passReqToCallback: true   // es para pedir mas datos de logueo

}, async (req, email, password, done) => {
  try {
    // primero busco el email para ver si esta regitrado
    let user = await User.findOne({ 'email': email })
    let leg = await User.findOne({ 'legajo': req.body.legajo })
    console.log(user)
    console.log(leg)
    if (user || leg) {  // si existe salgo
      return done(null, false, req.flash('signupMessage', 'Ops.. Ya tenemos registrado ese email/legajo')); // usso el flash para enviar mensajes
    } else {  //sino lo creo
      const newUser = new User(); // creo un nuveo usuario
      newUser.email = email;
      newUser.password = newUser.encryptPassword(password); //encrypto el dato
      newUser.nombre = req.body.nombre;
      newUser.apellido = req.body.apellido;
      newUser.gerencia = req.body.gerencia;
      newUser.nivel = 1;
      newUser.estaciones_fav = [];
      newUser.legajo = req.body.legajo;
      console.log("New User: " + newUser)
      await newUser.save();   // lo guardo
      done(null, newUser);  // callback para devolver la info. null: pq no hay error, y el usuario
    }
  } catch (error) {
    return done(null, false, req.flash('signupMessage', 'Ops.. Se produjo un error...')); // usso el flash para enviar mensajes
  }
}));

passport.use('local-signin', new LocalStrategy({   // ahora creo otro para loguearse
  usernameField: 'email',
  passwordField: 'password',
  passReqToCallback: true
}, async (req, email, password, done) => {
  try {
    const user = await User.findOne({ email: email });  //lo busco a ver si existe
    if (!user) {
      console.log("NO REGISTRADO")
      return done(null, false, req.flash('signinMessage', 'Ops.. No tenemos registrado este usuario...'));
    }
    if (!user.comparePassword(password)) {
      console.log("MAL PASS")
      return done(null, false, req.flash('signinMessage', 'Ops.. la contraseña es incorrecta.'));
    }
    return done(null, user);
  } catch (error) {
    return done(null, false, req.flash('signupMessage', 'Ops.. Se produjo un error...')); // usso el flash para enviar mensajes
  }
}));