module.exports = {
    mongodb: {
        //PARA USAR CON DOCKER
        URI: 'mongodb://mongo:27017/monitoreo-login-passport-local',
        URI_APILIMITER: 'mongodb://mongo:27017/api-limiter',
        URI_SIGNUPLIMITER: 'mongodb://mongo:27017/signup-limiter'

        //PARA USAR CON NPM
        // URI: 'mongodb://localhost:27017/monitoreo-login-passport-local',
        // URI_APILIMITER: 'mongodb://localhost:27017/api-limiter',
        // URI_SIGNUPLIMITER: 'mongodb://localhost:27017/signup-limiter'
    },
    sessions: {
        SECRETO: 'LOIU&YTRFVBNMKI*&^%$#WSDFGHJKOLP:'
    }
}
